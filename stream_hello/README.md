StreamHello
===========

1. Building and running it

```
$ mix compile
Compiled lib/task.ex
Compiled lib/stream_hello.ex
Generated stream_hello app
$ mix start
Listening on localhost:60000
```

2. Connecting to it

```
$ telnet localhost 60000
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
============
hello, world
============
1. Please input your name.
```

3. Communicating with it

```
============
hello, world
============
1. Please input your name.
tonyseek
hello, tonyseek

2. Please input your name.
world
hello, world

3. Please input your name.
```
