defmodule StreamHello do
  def start(port) do
    tcp_options = [:binary, {:packet, 0}, {:active, false}]
    case :gen_tcp.listen(port, tcp_options) do
      {:ok, socket} ->
        accept(socket)
      {:error, :eaddrinuse} ->
        IO.puts("Error occured: address is used by some other process\n")
    end
  end

  defp accept(socket) do
    {:ok, conn} = :gen_tcp.accept(socket)
    spawn(fn -> handle(conn, 0) end)
    accept(socket)
  end

  defp handle(conn, seq) do
    if seq == 0 do
      :gen_tcp.send(conn, "============\n")
      :gen_tcp.send(conn, "hello, world\n")
      :gen_tcp.send(conn, "============\n")
    end

    :gen_tcp.send(conn, to_string(seq + 1) <> ". Please input your name.\n")

    case :gen_tcp.recv(conn, 0) do
      {:ok, data} ->
        :gen_tcp.send(conn, "hello, " <> data <> "\n")
        handle(conn, seq + 1)
      {:error, :closed} ->
        :ok
    end
  end
end
