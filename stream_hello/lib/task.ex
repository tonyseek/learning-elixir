defmodule Mix.Tasks.Start do
  use Mix.Task

  def run(_) do
    port = 60000

    IO.puts("Listening on localhost:" <> to_string(port))
    StreamHello.start(port)
  end
end
